import setuptools
from distutils.core import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="tcli", # Replace with your own username
    version="0.0.5",
    author="tonder.org",
    author_email="info@tonder.org",
    description="A mqtt client for hosts",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/muad/tonder-client",
    packages=setuptools.find_packages(),
    scripts=['tbin/tcli', 'tbin/lox'],
    setup_requires=['wheel'],
    install_requires=['paho-mqtt', 'netifaces', 'pudb', 'urwid'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
