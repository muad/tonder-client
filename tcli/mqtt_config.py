import json
from pathlib import Path
from os import path, makedirs

class Config():

    def __init__(self):
        self.config = {}

    def _load(self):
        home = str(Path.home())
        self.datadir=f"{home}/.config/LoXTL9/data"
        if path.exists(f'{self.datadir}/config.json'):
            with open(f'{self.datadir}/config.json') as f:
                self.config = json.load(f)
        else:
            self.config = {
                "mqtt":{
                        "hostname":"local-mqtt",
                        "username":"bot",
                        "password":"password",
                        "topic":"testing_host",
                        "TLS":"False",
                        "port": 1883
                        }
                }

        return self.config

    def _update(self):
        self.config.update(self.new_data)
        if not path.exists(self.datadir):
            makedirs(self.datadir)
        with open(f'{self.datadir}/config.json', 'w') as f:
            json.dump(self.config, f)

    def get(self):
        """ fetches config for you
        return: config
        """
        return self._load()

    def set(self, new_data):
        """ saves the new data
        """
        self.new_data = new_data
        self._update()

if __name__ == "__main__":
    config = Config()
