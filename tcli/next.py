import urwid
import pudb
import math
from tcli.mqtt_config import Config

class QuestionBox(urwid.Filler):
    def keypress(self, size, key):
        if key != 'enter':
            return super(QuestionBox, self).keypress(size, key)
        self.original_widget = urwid.Text(
            u"Nice to meet you,\n%s.\n\nPress Q to exit." %
            tcli.edit.edit_text)

class BoxButton(urwid.WidgetWrap):
    _border_char = u'─'
    def __init__(self, label, on_press=None, user_data=None):
        padding_size = 2
        border = self._border_char * (len(label) + padding_size * 2)
        cursor_position = len(border) + padding_size

        self.top = u'┌' + border + u'┐\n'
        self.middle = u'│  ' + label + u'  │\n'
        self.bottom = u'└' + border + u'┘'

        # self.widget = urwid.Text([self.top, self.middle, self.bottom])
        self.widget = urwid.Pile([
            urwid.Text(self.top[:-1]),
            urwid.Text(self.middle[:-1]),
            urwid.Text(self.bottom),
        ])

        self.widget = urwid.AttrMap(self.widget, '', 'highlight')

        # self.widget = urwid.Padding(self.widget, 'center')
        # self.widget = urwid.Filler(self.widget)

        # here is a lil hack: use a hidden button for evt handling
        self._hidden_btn = urwid.Button('hidden %s' % label, on_press, user_data)

        super(BoxButton, self).__init__(self.widget)

    def selectable(self):
        return True

    def keypress(self, *args, **kw):
        return self._hidden_btn.keypress(*args, **kw)

    def mouse_event(self, *args, **kw):
        return self._hidden_btn.mouse_event(*args, **kw)

class CustomButton(urwid.Button):
    button_left = urwid.Text('[')
    button_right = urwid.Text(']')


class tcli(object):

    def __init__(self):
        self.title = "Welcome you low balling retards \n"
        self.palette=[('reversed', 'standout', '')]
        self.c = Config()
        config = self.c.get()
#       config = {
#           "mqtt":{
#                   "hostname":"local-mqtt",
#                   "username":"bot",
#                   "password":"password",
#                   "topic":"testing_host",
#                   "TLS":"False",
#                   "port": 1883
#                   }
#           }
        self.mqtt_hostname = config['mqtt']['hostname']
        self.mqtt_username = config['mqtt']['username']
        self.mqtt_port = int(config['mqtt']['port'])
        self.mqtt_password = config['mqtt']['password']
        self.mqtt_tls = config['mqtt']['TLS']
        self.mqtt_topic = config['mqtt']['topic']
        self.choices=[('config', self.mqtt_config), ('quit', self.exit_program), ('Chapman', self.item_chosen)]
        global main
        main = urwid.Padding(self.menu(), left=2, right=2)
        top = urwid.Overlay(main, urwid.SolidFill(u'\N{MEDIUM SHADE}'),
                            align='center', width=('relative', 90),
                            valign='middle', height=('relative', 90),
                            min_width=20, min_height=9)
        loop = urwid.MainLoop(top, self.palette)
        loop.run()

## return numbers of digits in a int
#   def get_count_digits(self, number: int):
#       """Return number of digits in a number."""

#       if number == 0:
#           return 1

#       number = abs(number)

#       if number <= 999999999999997:
#           return math.floor(math.log10(number)) + 1

#       count = 0
#       while number:
#           count += 1
#           number //= 10
#       return count

    def menu(self, *args):
        body = [urwid.Text(self.title), urwid.Divider()]
        for c in self.choices:
            title, select_function = c
            button = urwid.Button(title)
            urwid.connect_signal(button, 'click', select_function, title)
            body.append(urwid.AttrMap(button, None, focus_map='reversed'))
        if len(args) > 0:
            main.original_widget = urwid.ListBox(urwid.SimpleFocusListWalker(body))
        else:
            return urwid.ListBox(urwid.SimpleFocusListWalker(body))

    def custom_button(self, *args, **kwargs):
        b = CustomButton(*args, **kwargs)
        b = urwid.Padding(b, left=4, right=4)
        b = urwid.AttrMap(b, '', 'reversed')
        return b

    def quit_now(self):
        widget = urwid.Pile([
            urwid.Columns([
                self.custom_button('Quit', on_press=self.exit_program),
                self.custom_button('Cancel', on_press=self.menu),
            ]),
        ])
        #widget = urwid.Filler(widget, 'top')
        return widget

    def save_disgard(self):
        widget = urwid.Pile([
            urwid.Columns([
                self.custom_button('Save', on_press=self.save),
                self.custom_button('Disgard', on_press=self.menu),
            ]),
        ])
        #widget = urwid.Filler(widget, 'top')
        return widget

    def item_chosen(self, button, choice):
        #chosen = [urwid.Text(self.title), urwid.Divider()]
        response = urwid.Text([ choice, u' is a python character my god you are slow\n'])
        pile = self.quit_now()
        main.original_widget = urwid.Filler(urwid.Pile([response, pile]))

    def exit_program(self, button, *args, **kwargs):
        raise urwid.ExitMainLoop()

    def save(self, button):
        new_config = {"mqtt":{
            'hostname': self.mqtt_hostname,
            'username': self.mqtt_username,
            'port': self.mqtt_port,
            'password': self.mqtt_password,
            'TLS': self.mqtt_tls,
            'topic': self.mqtt_topic
        }}
        self.c.set(new_config)
        main.original_widget = self.menu()

    def on_mqtt_server_change(self, edit, new_edit_text):
        self.mqtt_hostname = new_edit_text

    def on_mqtt_username_change(self, edit, new_edit_text):
        self.mqtt_username = new_edit_text

    def on_mqtt_password_change(self, edit, new_edit_text):
        self.mqtt_password = new_edit_text

    def on_mqtt_port_change(self, edit, new_edit_text):
        self.mqtt_port = new_edit_text

    def on_mqtt_tls_change(self, edit, new_edit_text):
        self.mqtt_tls = new_edit_text

    def mqtt_config(self, *args, **kwargs):
        body = [urwid.Text(self.title), urwid.Divider()]
        self.response = urwid.Text([u'Let us configure the mqtt server connection'])
        mqtt_server = urwid.Edit(u"hostname: ", self.mqtt_hostname, edit_pos=len(self.mqtt_hostname)+1)
        mqtt_username = urwid.Edit("username: ", self.mqtt_username, edit_pos=len(self.mqtt_username)+1)
        mqtt_password = urwid.Edit("password: ", self.mqtt_password, edit_pos=len(self.mqtt_password)+1)
        mqtt_port = urwid.IntEdit("port: ", self.mqtt_port)
        urwid.connect_signal(mqtt_port, 'change', self.on_mqtt_port_change)
        mqtt_port = urwid.Padding(mqtt_port, left=4, right=4)
        mqtt_tls = urwid.Edit("TLS: ", self.mqtt_tls, edit_pos=len(self.mqtt_tls)+1)
        urwid.connect_signal(mqtt_tls, 'change', self.on_mqtt_tls_change)
        mqtt_tls = urwid.Padding(mqtt_tls, left=5)
        urwid.connect_signal(mqtt_server, 'change', self.on_mqtt_server_change)
        urwid.connect_signal(mqtt_username, 'change', self.on_mqtt_username_change)
        urwid.connect_signal(mqtt_password, 'change', self.on_mqtt_password_change)

        save_disgard = self.save_disgard()
        main.original_widget = urwid.Filler(urwid.Pile([self.response, urwid.Divider(), mqtt_server, mqtt_username, mqtt_password, mqtt_port, mqtt_tls, urwid.Divider(), save_disgard]))

if __name__ == '__main__':
    t = tcli()

