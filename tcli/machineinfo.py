#from netifaces import interfaces, ifaddresses, AF_INET
import netifaces
import socket
import json
import uuid

class MachineInfo:

    def __init__(self):
        self.data = {}
        self._get_hostname()
        self._get_machine_info()

    def _get_hostname(self):
        """
        Gets the hostname and FQDN for the host
        :returns: True
        """
        self.data['hostname'] = socket.gethostname()
        self.data['fqdn'] = socket.getfqdn()
        host_id = str(uuid.uuid3(uuid.NAMESPACE_DNS, self.data['fqdn']))
        self.data['host_id'] = host_id

    def _get_machine_info(self):
        """Gets the machine's info

        :returns: True
        """
        addresses = []
        self.data['interfaces'] = {}
        for interface in netifaces.interfaces():
            try:
                iface_data = netifaces.ifaddresses(interface)
                for family in iface_data:
                    if family not in (netifaces.AF_INET, netifaces.AF_INET6):
                        continue
                    for address in iface_data[family]:
                        addr = address['addr']

                        # If we have an ipv6 address remove the
                        # %ether_interface at the end
                        if family == netifaces.AF_INET6:
                            addr = addr.split('%')[0]
                        addresses.append(addr)
                if interface.startswith('lo'):
                    addresses = []
                else:
                    self.data['interfaces'][interface]=addresses
                    addresses = []

            except ValueError:
                pass

        return True

    def main(self):
        """
        Returns the data as json formatted string
        :returns: <class 'dict'> json formatted
        """
        return_data = {}
        return_data['hostdata'] = self.data
        return return_data

if __name__ == "__main__":
    adr = MachineInfo()
    data = adr.main()
    print(type(data))
    print(data)
