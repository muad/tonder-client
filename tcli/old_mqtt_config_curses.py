import curses
import time

## ABANDON SHIP THIS ONE IS SINKING! URWID! CHANGE TOO URWID!

class Config():

    def __init__(self):
        self.screen = curses.initscr()
        self.main()
        curses.noecho()
        curses.cbreak()
        self.screen.keypad(True)
        config = {
            "mqtt":{
                    "hostname":"local-mqtt",
                    "username":"bot",
                    "password":"password",
                    "topic":"testing_host",
                    "TLS":"False", 
                    "port": 1883
                    }
            }


    def get_param(self, x, y, string):
        """
        : return input string
        """
        #self.screen.clear()
        self.screen.border(0)
        self.screen.addstr(x, y, string)
        self.screen.refresh()
        input = self.screen.getstr(x, len(string)+y, 60)
        return input

    def ctxt(self, txt):
        """
        This function returns you x cordinate for the start of a centered line

        :return x
        """
        height, width = self.screen.getmaxyx()
        x = int((width // 2) - (len(txt) // 2) - len(txt) % 2)
        return x

    def menu(self):
        title = "Welcome to lox, how may I take your order today!?"
        self.screen.border(0)
        self.screen.addstr(2, self.ctxt(title), title)
        self.screen.addstr(4, 2, "MQTT Config let's start configuring the lox client")
        self.screen.refresh()

    def main(self):
        quit = True
        while quit:
            title = "Welcome to lox, how may I take your order today!?"
            self.screen.border(0)
            self.screen.addstr(2, self.ctxt(title), title)
            self.screen.addstr(4, 2, "MQTT Config let's start configuring the lox client")
            self.screen.refresh()
            mqtt_server = self.get_param(5, 2, "mqtt server: ")
            mqtt_server = self.get_param(6, 2, "testing: ")

            #self.menu()
            print(mqtt_server)
            time.sleep(1)
            quit = False

if __name__ == "__main__":
    config = Config()
