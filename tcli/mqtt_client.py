#import context
import paho.mqtt.client as mqtt
import logging
import pudb
import json


class DjangoMQTT:

    def __init__(self, config):
        #log = logging.basicConfig(level=logging.DEBUG)
        #self.logger = logging.getLogger(__name__)
        #self.logger.setLevel(logging.INFO)
        #handler = logging.FileHandler('irc/logs/mqtt.log')
        #self.context = bot
        self.log = logging.getLogger(__name__)
        # bot_config_nick =
        #config = self.bot.config[__name__]
        self.username = config['username']
        self.password = config['password']
        self.hostname = config['hostname']
        self.port = config['port']
        self.mqtt_TLS = config['TLS']

        self.nick = "Django_1"
        self.topic = f"host/{config['topic']}"
        self.log.info(f'Starting mqtt on bot {self.nick}')
        #self.disconnected = self.loop.create_future()
        self.got_message = None
        self.client = mqtt.Client()

        if self.username and self.password:
            self.log.debug(f"mqtt connecting with  {self.username}  {self.password}")
            self.client.username_pw_set(self.username, self.password)
        else:
            self.log.debug("mqtt running without username password")
            pudb.set_trace()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.on_disconnect = self.on_disconnect
        self.publish_data = ""

    def on_connect(self, mqttc, obj, flags, rc):
        '''
        0: Connection successful
        1: Connection refused incorrect protocol version
        2: Connection refused invalid client identifier
        3: Connection refused server unavailable
        4: Connection refused bad username or password
        5: Connection refused not authorised
        '''
        # TODO set connected flag in database
        #connection = int(self.client.loop())
        #if connection == 0:
        #    pudb.set_trace()
        #    self.log.info(f": MQTT : Subscribing: {self.topic}")
        #    self.client.subscribe(self.topic)            
        #elif connection == 5:
        #    pudb.set_trace()
        #    self.log.error(": MQTT : Connection refused not authorized")
        #else:
        #    pudb.set_trace()
        #    self.log.error(f": MQTT : Something bad has happend return code {rc}")
        #print("rc: "+str(rc))

    def on_message(self, mqttc, obj, msg):
        payload_decode=str(msg.payload.decode("utf-8","ignore"))
        try:
            json_data = json.loads(payload_decode)
        except ValueError as e:
            pudb.set_trace()
            self.log.error(f": MQTT : I did not get an json {e}")
        if msg.topic == "irc/event" and "irc_event" in json_data:
            pudb.set_trace()
            log_event = Event.objects.create(channel=json_data['irc_event']['channel'], 
                                                data=json_data['irc_event']['data'], 
                                                date=json_data['irc_event']['date'], 
                                                event=json_data['irc_event']['event'], 
                                                host=json_data['irc_event']['host'], 
                                                mask=json_data['irc_event']['mask'], 
                                                me=json_data['irc_event']['me'])

        print(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))

    def on_publish(self, mqttc, obj, mid):
        print("mid: "+str(mid))

    def on_subscribe(self, mqttc, obj, mid, granted_qos):
        print("Subscribed: "+str(mid)+" "+str(granted_qos))

    def on_log(self, mqttc, obj, level, string):
        print(string)
    
    def on_disconnect(self, client, userdata, rc):
        pudb.set_trace()
        # TODO unset connected flag in database
        #self.disconnected.set_result(rc)

    def run(self, hostdata):
#        self.connect("mqtt.eclipse.org", 1883, 60)
#        self.subscribe("$SYS/#", 0)
        if self.hostname:
            self.log.debug(f"LSW DEBUGGING: {self.hostname} {self.username} {self.password} {self.port}")

            if self.mqtt_TLS == "True":
                self.client.tls_set('/usr/local/share/certs/ca-root-nss.crt', tls_version=ssl.PROTOCOL_TLSv1_2)
            #self.client.tls_set(ca_certs="/etc/ssl/certs/")
            publish_data = json.dumps(hostdata)
            pudb.set_trace()
            self.client.connect(self.hostname, int(self.port), 60)
            self.client.subscribe(self.topic)
            self.client.publish(self.topic, publish_data, qos=1)

        else:
            pudb.set_trace()
            self.log.error("There is no mqtt to connect to!")

        rc = 0
        while rc == 0:
            rc = self.client.loop()
        return rc


# If you want to use a specific client id, use
# mqttc = MyMQTTClass("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.
#mqttc = MyMQTTClass()
#rc = mqttc.run()

#print("rc: "+str(rc)) 
if __name__ == "__main__":
    mqtt = DjangoMQTT()

