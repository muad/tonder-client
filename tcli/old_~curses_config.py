import curses


class Config():

    def __init__(self):
        self.screen = curses.initscr()
        self.main()

    def ctxt(self, txt):
        """
        This function returns you x cordinate for the start of a centered line

        :return x
        """
        height, width = self.screen.getmaxyx()
        x = int((width // 2) - (len(txt) // 2) - len(txt) % 2)
        return x

    def menu(self):
        title = "Welcome to tcli, how may I take your order today!?"
        self.screen.border(0)
        self.screen.addstr(2, self.ctxt(title), title)
        self.screen.addstr(4, 2, "1. MQTT Config")
        self.screen.refresh()

    def main(self):
        while True:
            self.menu()

if __name__ == "__main__":
    config = CurConfig()
