import urwid



class MenuButton(urwid.Button):
    def __init__(self, caption, callback):
        super(MenuButton, self).__init__("")
        urwid.connect_signal(self, 'click', callback)
        self._w = urwid.AttrMap(urwid.SelectableIcon(
            [u'  \N{BULLET} ', caption], 2), None, 'selected')

class SubMenu(urwid.WidgetWrap):
    def __init__(self, caption, choices):
        super(SubMenu, self).__init__(MenuButton(
            [caption, u"\N{HORIZONTAL ELLIPSIS}"], self.open_menu))
        line = urwid.Divider(u'\N{LOWER ONE QUARTER BLOCK}')
        listbox = urwid.ListBox(urwid.SimpleFocusListWalker([
            urwid.AttrMap(urwid.Text([u"\n  ", caption]), 'heading'),
            urwid.AttrMap(line, 'line'),
            urwid.Divider()] + choices + [urwid.Divider()]))
        self.menu = urwid.AttrMap(listbox, 'options')

    def open_menu(self, button): 
        tcli.top.open_box(self.menu)

class Choice(urwid.WidgetWrap):
    def __init__(self, caption):
        super(Choice, self).__init__(
            MenuButton(caption, self.item_chosen))
        self.caption = caption

    def item_chosen(self, button):
        response = urwid.Text([u'  You chose ', self.caption, u'\n'])
        done = MenuButton(u'Ok', self.warn)
        response_box = urwid.Filler(urwid.Pile([response, done]))
        tcli.top.open_box(urwid.AttrMap(response_box, 'options'))

    def warn(self, key):
        response = urwid.Text(['You are about too leave please confirm with q or Q \n'])
        response_box = urwid.Filler(urwid.Pile([response]))
        if key in ('q', 'Q', 'esc'):
            self.exit_program 
        else:
            pass
        tcli.top.open_box(urwid.AttrMap(response_box, 'options'))

    def exit_program(self, key):
        raise urwid.ExitMainLoop()

class HorizontalBoxes(urwid.Columns):
    def __init__(self):
        super(HorizontalBoxes, self).__init__([], dividechars=1)

    def open_box(self, box):
        if self.contents:
            del self.contents[self.focus_position + 1:]
        self.contents.append((urwid.AttrMap(box, 'options', tcli.focus_map),
            self.options('given', 24)))
        self.focus_position = len(self.contents) - 1

class tcli():
    menu_top = SubMenu(u'Main Menu', [
        SubMenu(u'Applications', [
            SubMenu(u'Accessories', [
                Choice(u'Text Editor'),
                Choice(u'Terminal'),
            ]),
        ]),
        SubMenu(u'System', [
            SubMenu(u'Preferences', [
                Choice(u'Appearance'),
            ]),
            Choice(u'Lock Screen'),
        ]),
    ])

    palette = [
        (None,  'light gray', 'black'),
        ('heading', 'black', 'light gray'),
        ('line', 'black', 'light gray'),
        ('options', 'dark gray', 'black'),
        ('focus heading', 'white', 'dark red'),
        ('focus line', 'black', 'dark red'),
        ('focus options', 'black', 'light gray'),
        ('selected', 'white', 'dark blue')]
    focus_map = {
        'heading': 'focus heading',
        'options': 'focus options',
        'line': 'focus line'}

    top = HorizontalBoxes()
    
    def __init__(self):
        tcli.top.open_box(tcli.menu_top.menu)
        urwid.MainLoop(urwid.Filler(tcli.top, 'middle', 10), tcli.palette).run()

if __name__ == "__main__":
    t = tcli()
